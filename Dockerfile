FROM lachlanevenson/k8s-helm:v2.12.3

ENV S3_PLUGIN_VERSION="0.8.0"

RUN apk add --update ca-certificates \
 && apk add --update -t deps git openssh bash \
 && helm init --client-only \
 && helm plugin install https://github.com/hypnoglow/helm-s3.git --version ${S3_PLUGIN_VERSION} \
 && apk del --purge deps \
 && rm /var/cache/apk/* 